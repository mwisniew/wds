#include "crc16.hh"

void CRC::calcByte(const uint8_t & byte){

    crc ^= (uint16_t)byte << 8;
    for(size_t i = 0; i < 8; i++){
        if(crc & 0x8000)
            crc = (crc << 1) ^ PN;
        else
            crc <<= 1;
    }

}

void CRC::calcByteArray(const char* array, uint16_t length){
    while(length--)
        calcByte(*(array++));

}
