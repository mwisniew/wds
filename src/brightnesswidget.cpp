#include "brightnesswidget.hh"

BrightnessWidget::BrightnessWidget(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
    frame->setStyleSheet("background-color: #d7d6d5");
}

void BrightnessWidget::display(int val){
    value->setText(QString::number(val));

    QPixmap original("://img/light.png");


    QPixmap result(original.size());
    result.fill(Qt::transparent);

    QPainter painter;

    painter.begin(&result);
    painter.setOpacity(((double)val) / 100.0);
    painter.drawPixmap(0, 0, original);
    painter.end();

    light->setPixmap(result);
}
