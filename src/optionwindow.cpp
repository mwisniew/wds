
#include "optionwindow.hh"

OptionWindow::OptionWindow(QWidget *parent ):
    QDialog(parent),
    m_intValidator(new QIntValidator(0, 4000000, this))
{
    setupUi(this);

    connect(applyButton, &QPushButton::clicked, this, &OptionWindow::apply);
    connect(serialportCombo, &QComboBox::currentIndexChanged, this, &OptionWindow::showPortInfo);
    connect(baudrateCombo,  &QComboBox::currentIndexChanged, this, &OptionWindow::checkCustomBaudRatePolicy);
    connect(serialportCombo, &QComboBox::currentIndexChanged, this, &OptionWindow::checkCustomDevicePathPolicy);
    connect(languageCombo, &QComboBox::currentIndexChanged, this, &OptionWindow::langChangeSlot);

    fillPortsParameters();
    fillPortsInfo();

    updateSettings();
}

OptionWindow::~OptionWindow()
{
}

OptionWindow::Settings OptionWindow::settings() const
{
    return m_currentSettings;
}

void OptionWindow::showPortInfo(int idx)
{
    if (idx == -1)
        return;

    const QString blankString = tr("N/A");

    const QStringList list = serialportCombo->itemData(idx).toStringList();
    descriptionLabel->setText(tr("Opis: %1").arg(list.value(1, blankString)));
    manufacturerLabel->setText(tr("Producent: %1").arg(list.value(2, blankString)));
    serialNumLabel->setText(tr("Numer seryjny: %1").arg(list.value(3, blankString)));
}

void OptionWindow::apply()
{
    updateSettings();
    hide();
}

void OptionWindow::checkCustomBaudRatePolicy(int idx)
{
    const bool isCustomBaudRate = !baudrateCombo->itemData(idx).isValid();
    baudrateCombo->setEditable(isCustomBaudRate);
    if (isCustomBaudRate) {
        baudrateCombo->clearEditText();
        QLineEdit *edit = baudrateCombo->lineEdit();
        edit->setValidator(m_intValidator);
    }
}

void OptionWindow::checkCustomDevicePathPolicy(int idx)
{
    const bool isCustomPath = !serialportCombo->itemData(idx).isValid();
    serialportCombo->setEditable(isCustomPath);
    if (isCustomPath)
        serialportCombo->clearEditText();
}

void OptionWindow::fillPortsParameters()
{

    baudrateCombo->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    baudrateCombo->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    baudrateCombo->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    baudrateCombo->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    baudrateCombo->addItem(QApplication::translate("OptionWindow","Inna", nullptr));

    databitsCombo->addItem(QStringLiteral("5"), QSerialPort::Data5);
    databitsCombo->addItem(QStringLiteral("6"), QSerialPort::Data6);
    databitsCombo->addItem(QStringLiteral("7"), QSerialPort::Data7);
    databitsCombo->addItem(QStringLiteral("8"), QSerialPort::Data8);

    databitsCombo->setCurrentIndex(3);


    parityCombo->addItem(QApplication::translate("OptionWindow","None", nullptr), QSerialPort::NoParity);
    parityCombo->addItem(QApplication::translate("OptionWindow","Even", nullptr), QSerialPort::EvenParity);
    parityCombo->addItem(QApplication::translate("OptionWindow","Odd", nullptr), QSerialPort::OddParity);
    parityCombo->addItem(QApplication::translate("OptionWindow","Mark", nullptr), QSerialPort::MarkParity);
    parityCombo->addItem(QApplication::translate("OptionWindow","Space", nullptr), QSerialPort::SpaceParity);

    stopbitsCombo->addItem(QStringLiteral("1"), QSerialPort::OneStop);
#ifdef Q_OS_WIN
    stopbitsCombo->addItem(tr("1.5"), QSerialPort::OneAndHalfStop);
#endif
    stopbitsCombo->addItem(QStringLiteral("2"), QSerialPort::TwoStop);

    controlCombo->addItem(QApplication::translate("OptionWindow","Brak", nullptr), QSerialPort::NoFlowControl);
    controlCombo->addItem(QApplication::translate("OptionWindow","RTS/CTS", nullptr), QSerialPort::HardwareControl);
    controlCombo->addItem(QApplication::translate("OptionWindow","XOFF/XON", nullptr), QSerialPort::SoftwareControl);
}

void OptionWindow::fillPortsInfo()
{
    serialportCombo->clear();
    const QString blankString = tr("N/A");
    const QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();

    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        const QString description = info.description();
        const QString manufacturer = info.manufacturer();
        const QString serialNumber = info.serialNumber();
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << (!serialNumber.isEmpty() ? serialNumber : blankString)
             << info.systemLocation();

       serialportCombo->addItem(list.constFirst(), list);
    }

    serialportCombo->addItem(tr("..."));
}

void OptionWindow::updateSettings()
{
    m_currentSettings.name = serialportCombo->currentText();

    if (baudrateCombo->currentIndex() == 4) {
       m_currentSettings.baudRate = baudrateCombo->currentText().toInt();
    } else {
       const QVariant baudRateData = baudrateCombo->currentData();
       m_currentSettings.baudRate = baudRateData.value<QSerialPort::BaudRate>();
    }
    m_currentSettings.stringBaudRate = QString::number(m_currentSettings.baudRate);

    const QVariant dataBitsData = databitsCombo->currentData();
    m_currentSettings.dataBits = dataBitsData.value<QSerialPort::DataBits>();
    m_currentSettings.stringDataBits = databitsCombo->currentText();

    const QVariant parityData = parityCombo->currentData();
    m_currentSettings.parity = parityData.value<QSerialPort::Parity>();
    m_currentSettings.stringParity = parityCombo->currentText();

    const QVariant stopBitsData = stopbitsCombo->currentData();
    m_currentSettings.stopBits = stopBitsData.value<QSerialPort::StopBits>();
    m_currentSettings.stringStopBits = stopbitsCombo->currentText();

    const QVariant flowControlData = controlCombo->currentData();
    m_currentSettings.flowControl = flowControlData.value<QSerialPort::FlowControl>();
    m_currentSettings.stringFlowControl = controlCombo->currentText();

}

void OptionWindow::langChangeSlot(int idx){
    emit languageChange(idx);
}

void OptionWindow::translateUI(){
    retranslateUi(this);

    baudrateCombo->clear();
    databitsCombo->clear();
    parityCombo->clear();
    stopbitsCombo->clear();
    controlCombo->clear();

    fillPortsParameters();
    fillPortsInfo();
}
