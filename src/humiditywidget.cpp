#include "humiditywidget.hh"

HumidityWidget::HumidityWidget(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(handleTimeout()));
    m_timer.setInterval(100);
    m_opacity = 0.30;
    m_step = 25;
    m_timer.start();
    value->setStyleSheet("background-color: #d7d6d5");
    valText->setStyleSheet("background-color: #d7d6d5");
    frame->setStyleSheet("background-color: #d7d6d5");
}

void HumidityWidget::display(int val){
    value->setText(QString::number(val));

    if(val < m_step){
        fog1->hide();
        fog2->hide();
        fog3->hide();
        return;
    }

    if(val > m_step){
        fog1->show();
    } else fog1->hide();

    if(val > 2 * m_step){
        fog2->show();
    } else fog2->hide();

    if(val > 3 * m_step){
        fog3->show();
    } else fog3->hide();
}

void HumidityWidget::handleTimeout(){
    QPixmap original("://img/fog.png");

    if(m_opacity >= 0.5){
        m_increaseOpacity = false;
    }
    if(m_opacity <= 0.0){
        m_increaseOpacity = true;
    }

    if(m_increaseOpacity){
        m_opacity += 0.05;
    } else m_opacity -= 0.05;

    QPixmap result1(original.size());
    QPixmap result2(original.size());
    QPixmap result3(original.size());
    result1.fill(Qt::transparent);
    result2.fill(Qt::transparent);
    result3.fill(Qt::transparent);
    QPainter painter;

    painter.begin(&result1);
    painter.setOpacity(0.5 + m_opacity);
    painter.drawPixmap(0, 0, original);
    painter.end();

    painter.begin(&result2);
    painter.setOpacity(1.0 - m_opacity);
    painter.drawPixmap(0, 0, original);
    painter.end();

    painter.begin(&result3);
    painter.setOpacity(0.5 + m_opacity / 2.0);
    painter.drawPixmap(0, 0, original);
    painter.end();

    fog1->setPixmap(result1);
    fog2->setPixmap(result2);
    fog3->setPixmap(result3);
}
