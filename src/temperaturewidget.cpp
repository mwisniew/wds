#include "temperaturewidget.hh"


TemperatureWidget::TemperatureWidget(QWidget *parent)
    : QWidget(parent),
    m_max(45.0),
    m_min(15.0),
    m_offset(232.5),
    m_mult(-4.17)

{
    setupUi(this);
    //value->setStyleSheet("background-color: #d7d6d5");
    //label->setStyleSheet("background-color: #d7d6d5");
}

void TemperatureWidget::display(double val){
    value->setText(QString::number(val));
    if(val > m_max){
        bar->setFixedHeight(55);
        return;
    }

    if(val < m_min){
        bar->setFixedHeight(200);
        return;
    }

    int height = m_mult * val + m_offset;
    bar->setFixedHeight(height);
}
