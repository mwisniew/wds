
#include "mainwindow.hh"
#include <chrono>


static constexpr std::chrono::seconds kWriteTimeout = std::chrono::seconds{5};

int i =0;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    m_timer(new QTimer(this)),
    m_serial(new QSerialPort(this)),
    m_settings(new OptionWindow(this))
{
    setupUi(this);



    HeaterPower->setStep(50.0);
    LampPower->setStep(10.0);

    connect(this, SIGNAL(temperatureValueChanged(double)), TemperatureDisplay, SLOT(display(double)));
    connect(this, SIGNAL(humidityValueChanged(int)), HumidityDisplay, SLOT(display(int)));
    connect(this, SIGNAL(lightValueChanged(int)), BrightnessDisplay, SLOT(display(int)));

    connect(this, SIGNAL(heaterValueChanged(double)), HeaterPower, SLOT(display(double)));
    connect(this, SIGNAL(humidifierStateChanged(int)), HumiState, SLOT(displayState(int)));
    connect(this, SIGNAL(lampValueChanged(double)), LampPower, SLOT(display(double)));

    connect(ConnectButton, SIGNAL(clicked()), this, SLOT(openSerialPort()));
    connect(DisconnectButton, SIGNAL(clicked()), this, SLOT(closeSerialPort()));
    connect(optionsButton, SIGNAL(clicked()), m_settings, SLOT(show()));

    connect(m_serial, &QSerialPort::readyRead, this, &MainWindow::readData);
    connect(m_serial, &QSerialPort::bytesWritten, this, &MainWindow::handleBytesWritten);

    std::vector<QLayout*> chartLayouts;
    std::vector<QChartView*> chartViews;
    std::vector<double> y_max = {100.0, 1.0, 70.0, 100.0, 100.0, 50.0};
    std::vector<double> y_min = {20.0, 0.0, 10.0, 50.0, 0.0, 10.0};

    char chartNames[6][32]={
        QT_TR_NOOP("Moc lampy"),
        QT_TR_NOOP("Stan nawilżacza"),
        QT_TR_NOOP("Moc grzałki"),
        QT_TR_NOOP("Naświetlenie"),
        QT_TR_NOOP("Wilgotność"),
        QT_TR_NOOP("Temperatura")

    };

    for(size_t chart_ind = 0; chart_ind < 6; chart_ind++){
        m_charts.push_back(new Chart(tr(chartNames[chart_ind])));
        m_chartWindows.push_back(new QDialog(this));

        m_charts[chart_ind]->setAnimationOptions(QChart::AllAnimations);
        m_charts[chart_ind]->changeRanges(0, 10, y_min[chart_ind], y_max[chart_ind]);

        chartViews.push_back(new QChartView(m_charts[chart_ind]));

        chartLayouts.push_back(new QGridLayout);
        chartLayouts[chart_ind]->addWidget(chartViews[chart_ind]);

        m_chartWindows[chart_ind]->setLayout(chartLayouts[chart_ind]);
    }

    connect(lampPowerChartButton, SIGNAL(clicked()), m_chartWindows[0], SLOT(show()));
    connect(humidifierStateChartButton, SIGNAL(clicked()), m_chartWindows[1], SLOT(show()));
    connect(heaterPowerChartButton, SIGNAL(clicked()), m_chartWindows[2], SLOT(show()));
    connect(lightChartButton, SIGNAL(clicked()), m_chartWindows[3], SLOT(show()));
    connect(humidityChartButton, SIGNAL(clicked()), m_chartWindows[4], SLOT(show()));
    connect(temperatureChartButton, SIGNAL(clicked()), m_chartWindows[5], SLOT(show()));


    connect(this, SIGNAL(lampValueChanged(double)), m_charts[0], SLOT(newData(double)));
    connect(this, SIGNAL(humidifierStateChanged(int)), m_charts[1], SLOT(newData(int)));
    connect(this, SIGNAL(heaterValueChanged(double)), m_charts[2], SLOT(newData(double)));
    connect(this, SIGNAL(lightValueChanged(int)), m_charts[3], SLOT(newData(int)));
    connect(this, SIGNAL(humidityValueChanged(int)), m_charts[4], SLOT(newData(int)));
    connect(this, SIGNAL(temperatureValueChanged(double)), m_charts[5], SLOT(newData(double)));

    connect(m_settings, SIGNAL(languageChange(int)), this, SLOT(handleLanguageChange(int)));
}

MainWindow::~MainWindow()
{
}

void MainWindow::openSerialPort()
{
    const OptionWindow::Settings p = m_settings->settings();
    m_serial->setPortName(p.name);
    m_serial->setBaudRate(p.baudRate);
    m_serial->setDataBits(p.dataBits);
    m_serial->setParity(p.parity);
    m_serial->setStopBits(p.stopBits);
    m_serial->setFlowControl(p.flowControl);

    m_serial->open(QIODevice::ReadWrite);

}


void MainWindow::closeSerialPort(){
    if (m_serial->isOpen())
        m_serial->close();
}

void MainWindow::writeData(const QByteArray &data){
    const qint64 written = m_serial->write(data);
    if (written == data.size()) {
        m_bytesToWrite += written;
        m_timer->start(kWriteTimeout);
    }
}

void MainWindow::readData(){

    if(m_busyPort.try_lock() == true){
        Measurements frame;
        std::future<Measurements> framePromise = std::async(&MainWindow::readFrame, this);
        std::string crc_data;
        CRC crc_calc;

        while(framePromise.wait_for(std::chrono::seconds(0)) != std::future_status::ready){
            QCoreApplication::processEvents();
        }

        frame = framePromise.get();

        crc_data = std::to_string(frame.m_temperature) + std::to_string(frame.m_humidity)
                   + std::to_string(frame.m_brightness) + std::to_string(frame.m_heater_control)
                   + std::to_string(frame.m_humi_state) + std::to_string(frame.m_lamp_control);

        crc_calc.calcByteArray(crc_data.data(), crc_data.size());



        //if(crc_received == crc_calc.getCRC()){
        if(frame.m_correct){
            emit temperatureValueChanged(frame.m_temperature);
            emit humidityValueChanged(frame.m_humidity);
            emit lightValueChanged(frame.m_brightness);
            emit heaterValueChanged(frame.m_heater_control);
            emit humidifierStateChanged(frame.m_humi_state);
            emit lampValueChanged(frame.m_lamp_control);
        }
        //}

        m_busyPort.unlock();
    }
}

void MainWindow::handleError(QSerialPort::SerialPortError error){
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), m_serial->errorString());
        closeSerialPort();
    }
}

void MainWindow::handleBytesWritten(qint64 bytes){
    m_bytesToWrite -= bytes;
    if (m_bytesToWrite == 0)
        m_timer->stop();
}

void MainWindow::handleWriteTimeout(){

}

Measurements MainWindow::readFrame(){
    Measurements frame;
    std::chrono::milliseconds pause(10);
    char tmp;


    while(!m_serial->canReadLine())
        std::this_thread::sleep_for(pause);

    const QByteArray data = m_serial->readLine(100);
    QTextStream stream(data);
    stream.setRealNumberPrecision(2);

    stream >> tmp;
    if(tmp != 'X'){
        frame.m_correct = false;
        return frame;
    }



    stream.setStatus(QTextStream::Ok);
    stream >> frame.m_temperature >> frame.m_humidity >> frame.m_brightness;
    stream >> frame.m_heater_control >> frame.m_humi_state >> frame.m_lamp_control;
    stream >> frame.m_crc_received;

    if(stream.status() != QTextStream::Ok){
        frame.m_correct = false;
        return frame;
    }

    stream.skipWhiteSpace();
    stream >> tmp;

    if(tmp != 'F'){
        frame.m_correct = false;
        return frame;
    }

    frame.m_correct = true;
    return frame;

}

void MainWindow::handleLanguageChange(int ind){
    static QTranslator* translator = new QTranslator();

    switch(ind) {
        case 0:
            qApp->removeTranslator(translator);
            break;

        case 1:
            if(translator->load("app_en")){
                qApp->installTranslator(translator);

            } else std::cerr << "Error: File app_en.qm not loaded!" << QDir::currentPath().toStdString() << std::endl;
            break;
    }

}

void MainWindow::changeEvent(QEvent* event){
    if(event->type() == QEvent::LanguageChange){
        retranslateUi(this);

        return;
    }

    QMainWindow::changeEvent(event);

}

void MainWindow::paintEvent(QPaintEvent *pEvent){

    QString lpwr = tr("Moc lampy");
    QString hst = tr("Stan nawilżacza");
    QString hpwr = tr("Moc grzałki");
    QString lig = tr("Naświetlenie");
    QString hum = tr("Wilgotność");
    QString tem = tr("Temperatura");

    lpwr = QApplication::translate("MainWindow", lpwr.toStdString().c_str(), nullptr);
    hst = QApplication::translate("MainWindow", hst.toStdString().c_str(), nullptr);
    hpwr = QApplication::translate("MainWindow", hpwr.toStdString().c_str(), nullptr);
    lig = QApplication::translate("MainWindow", lig.toStdString().c_str(), nullptr);
    hum = QApplication::translate("MainWindow", hum.toStdString().c_str(), nullptr);
    tem = QApplication::translate("MainWindow", tem.toStdString().c_str(), nullptr);

    m_charts[0]->setName(lpwr);
    m_charts[1]->setName(hst);
    m_charts[2]->setName(hpwr);
    m_charts[3]->setName(lig);
    m_charts[4]->setName(hum);
    m_charts[5]->setName(tem);

    QMainWindow::paintEvent(pEvent);
}
