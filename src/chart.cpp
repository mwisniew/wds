#include "chart.hh"

using time_point = std::chrono::time_point<std::chrono::steady_clock>;

Chart::Chart(const QString & name, QGraphicsItem *parent, Qt::WindowFlags wFlags):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    m_series(nullptr),
    m_axisX(new QValueAxis()),
    m_axisY(new QValueAxis()),
    m_x(0),
    m_y(0),
    m_prevDataTime(std::chrono::steady_clock::now()),
    m_start(std::chrono::steady_clock::now())
{
    m_series = new QSplineSeries(this);

    m_series->setName(name);

    QPen blue_pen(Qt::blue);
    blue_pen.setWidth(3);

    m_series->setPen(blue_pen);
    m_series->append(m_x, m_y);

    addSeries(m_series);

    addAxis(m_axisX,Qt::AlignBottom);
    addAxis(m_axisY,Qt::AlignLeft);
    m_series->attachAxis(m_axisX);
    m_series->attachAxis(m_axisY);
    m_axisX->setRange(0, 30);
    m_axisY->setRange(-10, 10);
}

Chart::~Chart()
{

}
void Chart::changeRanges(const double & min_x, const double & max_x, const double & min_y, const double & max_y){
    m_axisX->setRange(min_x, max_x);
    m_axisY->setRange(min_y, max_y);
}

void Chart::newData(double data)//_x, double data_y)
{
    time_point currTime = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = currTime - m_prevDataTime;
    double step = plotArea().width() / m_axisX->tickCount();
    diff = currTime - m_start;
    m_x += diff.count();
    m_y = data;

    m_series->append(m_x, m_y);
    scroll(step, 0);
    m_prevDataTime = currTime;
}

void Chart::newData(int data)//_x, double data_y)
{
    time_point currTime = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = currTime - m_prevDataTime;
    double step = plotArea().width() / m_axisX->tickCount();
    diff = currTime - m_start;
    m_x += diff.count();
    m_y = data;

    m_series->append(m_x, m_y);
    scroll(step, 0);
    m_prevDataTime = currTime;
}

void Chart::setName(const QString & name){
    m_series->setName(name);
}
