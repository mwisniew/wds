#include "displaywidget.hh"

DisplayWidget::DisplayWidget(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
    value->setText(tr("0.0"));
}


void DisplayWidget::display(double val){
    value->setText(QString::number(val));

    if(val > m_step / 10.0){
        topright->setPixmap(QPixmap("://img/disp_seg_on_1.png"));
    } else topright->setPixmap(QPixmap("://img/disp_seg_off_1.png"));

    if(val > m_step * 1.0){
        botright->setPixmap(QPixmap("://img/disp_seg_on_2.png"));
    } else botright->setPixmap(QPixmap("://img/disp_seg_off_2.png"));

    if(val > m_step * 2.0){
        botleft->setPixmap(QPixmap("://img/disp_seg_on_3.png"));
    } else botleft->setPixmap(QPixmap("://img/disp_seg_off_3.png"));

    if(val > m_step * 3.0){
        topleft->setPixmap(QPixmap("://img/disp_seg_on_4.png"));
    } else topleft->setPixmap(QPixmap("://img/disp_seg_off_4.png"));
}

void DisplayWidget::displayState(int val){
    if(val > 0){
        value->setText(tr("ON"));
        topright->setPixmap(QPixmap("://img/disp_seg_on_1.png"));
        botright->setPixmap(QPixmap("://img/disp_seg_on_2.png"));
        botleft->setPixmap(QPixmap("://img/disp_seg_on_3.png"));
        topleft->setPixmap(QPixmap("://img/disp_seg_on_4.png"));
    } else {
        value->setText(tr("OFF"));
        topright->setPixmap(QPixmap("://img/disp_seg_off_1.png"));
        botright->setPixmap(QPixmap("://img/disp_seg_off_2.png"));
        botleft->setPixmap(QPixmap("://img/disp_seg_off_3.png"));
        topleft->setPixmap(QPixmap("://img/disp_seg_off_4.png"));
    }

}
