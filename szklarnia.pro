QT       += core gui widgets charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets serialport

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
DEPENDPATH += src
INCLUDEPATH += hdr
TRANSLATIONS = app_pl.ts
TRANSLATIONS += app_en.ts

SOURCES += \
    src/brightnesswidget.cpp \
    src/chart.cpp \
    src/displaywidget.cpp \
    src/humiditywidget.cpp \
    src/optionwindow.cpp \
    src/crc16.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/temperaturewidget.cpp

HEADERS += \
    hdr/brightnesswidget.hh \
    hdr/chart.hh \
    hdr/crc16.hh \
    hdr/displaywidget.hh \
    hdr/humiditywidget.hh \
    hdr/mainwindow.hh \
    hdr/measurements.hh \
    hdr/optionwindow.hh \
    hdr/temperaturewidget.hh \
    hdr/translationsmacros.hh

FORMS += \
    brightnesswidget.ui \
    displaywidget.ui \
    fogwidget.ui \
    mainwindow.ui \
    optionwindow.ui \
    thermometer.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
