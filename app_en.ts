<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>BrightnessWidget</name>
    <message>
        <location filename="brightnesswidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="brightnesswidget.ui" line="85"/>
        <source>Jasność:</source>
        <translation>Brightness:</translation>
    </message>
    <message>
        <location filename="brightnesswidget.ui" line="92"/>
        <source>0</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DisplayWidget</name>
    <message>
        <location filename="displaywidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="displaywidget.ui" line="26"/>
        <source>val</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/displaywidget.cpp" line="7"/>
        <source>0.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/displaywidget.cpp" line="33"/>
        <source>ON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/displaywidget.cpp" line="39"/>
        <source>OFF</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FogWidget</name>
    <message>
        <location filename="fogwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="fogwidget.ui" line="113"/>
        <source>Wilgotność:</source>
        <translation>Humidity:</translation>
    </message>
    <message>
        <location filename="fogwidget.ui" line="129"/>
        <source>0</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <location filename="src/mainwindow.cpp" line="44"/>
        <location filename="src/mainwindow.cpp" line="247"/>
        <source>Moc lampy</source>
        <translation>Lamp power</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <location filename="mainwindow.ui" line="173"/>
        <location filename="mainwindow.ui" line="236"/>
        <location filename="mainwindow.ui" line="447"/>
        <location filename="mainwindow.ui" line="460"/>
        <location filename="mainwindow.ui" line="473"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="163"/>
        <location filename="src/mainwindow.cpp" line="45"/>
        <location filename="src/mainwindow.cpp" line="248"/>
        <source>Stan nawilżacza</source>
        <translation>Humidifier state</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="210"/>
        <location filename="src/mainwindow.cpp" line="46"/>
        <location filename="src/mainwindow.cpp" line="249"/>
        <source>Moc grzałki</source>
        <translation>Heater power</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <source>Zadaj naświetlenie</source>
        <translation>Set brightness</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="296"/>
        <source>Zadaj wilgotność</source>
        <translation>Set humidity</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="323"/>
        <source>Zadaj temperaturę</source>
        <translation>Set temperature</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="360"/>
        <source>Połącz</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="373"/>
        <source>Rozłącz</source>
        <translation>Disconnect</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="386"/>
        <source>Opcje</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="47"/>
        <location filename="src/mainwindow.cpp" line="250"/>
        <source>Naświetlenie</source>
        <translation>Brightness</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="48"/>
        <location filename="src/mainwindow.cpp" line="251"/>
        <source>Wilgotność</source>
        <translation>Humidity</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="49"/>
        <location filename="src/mainwindow.cpp" line="252"/>
        <source>Temperatura</source>
        <translation>Temperature</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="157"/>
        <source>Critical Error</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>OptionWindow</name>
    <message>
        <location filename="optionwindow.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="167"/>
        <source>Zmiana języka</source>
        <translation>Language settings</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="173"/>
        <source>Język:</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="181"/>
        <source>Polski</source>
        <translation>Polish</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="186"/>
        <source>Angielski</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="197"/>
        <source>Zastosuj</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="20"/>
        <source>Wybór Portu Szeregowego</source>
        <translation>Serial port selection</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="29"/>
        <source>Opis:</source>
        <translation>Description:</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="36"/>
        <source>Producent:</source>
        <translation>Manufacturer:</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="43"/>
        <source>Numer seryjny:</source>
        <translation>Serial number:</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="53"/>
        <source>Wybór Parametrów</source>
        <translation>Parameters</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="59"/>
        <source>Baudrate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="72"/>
        <source>Bity danych</source>
        <translation>Data bits</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="82"/>
        <source>Parzystość</source>
        <translation>Parity</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="92"/>
        <source>Bity stopu</source>
        <translation>Stop bits</translation>
    </message>
    <message>
        <location filename="optionwindow.ui" line="102"/>
        <source>Kontrola</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="36"/>
        <location filename="src/optionwindow.cpp" line="106"/>
        <source>N/A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="39"/>
        <source>Opis: %1</source>
        <translation>Description: %1</translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="40"/>
        <source>Producent: %1</source>
        <translation>Manufacturer: %1</translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="41"/>
        <source>Numer seryjny: %1</source>
        <translation>Serial number: %1</translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="76"/>
        <source>Inna</source>
        <translation>Custom</translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="86"/>
        <source>None</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="87"/>
        <source>Even</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="88"/>
        <source>Odd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="89"/>
        <source>Mark</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="90"/>
        <source>Space</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="94"/>
        <source>1.5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="98"/>
        <source>Brak</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="99"/>
        <source>RTS/CTS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="100"/>
        <source>XOFF/XON</source>
        <translation></translation>
    </message>
    <message>
        <location filename="src/optionwindow.cpp" line="123"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <source>Inne</source>
        <translation type="vanished">Custom</translation>
    </message>
</context>
<context>
    <name>Thermometer</name>
    <message>
        <location filename="thermometer.ui" line="20"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="thermometer.ui" line="73"/>
        <source>21.3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="thermometer.ui" line="83"/>
        <source>°C</source>
        <translation></translation>
    </message>
</context>
</TS>
