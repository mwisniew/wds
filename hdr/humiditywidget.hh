
#ifndef HUMIDITYWIDGET_HH
#define HUMIDITYWIDGET_HH

#include <QWidget>
#include <QTimer>
#include <QPainter>
#include "ui_fogwidget.h"
#include "translationsmacros.hh"

QT_BEGIN_NAMESPACE
namespace Ui { class FogWidget; }
QT_END_NAMESPACE

class HumidityWidget : public QWidget, private Ui::FogWidget
{
Q_OBJECT
MAKE_TRANSLATABLE(QWidget)
public:
    /** \brief Humidity widget constructor
     *
     * \param parent - pointer to widget's parent
     *
     */
    HumidityWidget(QWidget *parent = nullptr);

    /** \brief Destructor
     *
     */
    ~HumidityWidget() {}

public slots:
    /** \brief Display new value and change fog amount on widget
     *
     * \param val - new value to be displayed
     *
     */
    void display(int val);


private slots:
    void handleTimeout();

private:
    QTimer m_timer;
    double m_opacity;
    int m_step;
    bool m_increaseOpacity;

};

#endif // HUMIDITYWIDGET_HH
