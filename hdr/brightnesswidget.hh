
#ifndef BRIGHTNESSWIDGET_H
#define BRIGHTNESSWIDGET_H

#include <QWidget>
#include <QPainter>
#include "ui_brightnesswidget.h"
#include "translationsmacros.hh"

QT_BEGIN_NAMESPACE
namespace Ui { class BrightnessWidget; }
QT_END_NAMESPACE

class BrightnessWidget : public QWidget, private Ui::BrightnessWidget
{
    MAKE_TRANSLATABLE(QWidget)
    Q_OBJECT
public:
    /** \brief Brightness display widget constructor
     *
     * \param parent - pointer to widget's parent
     *
     */
    BrightnessWidget(QWidget *parent = nullptr);

    /** \brief Destructor
     *
     */
    ~BrightnessWidget() {}

public slots:
    /** \brief Display new value and change light beam opacity
     *
     * \param val - new value to be displayed
     *
     */
    void display(int val);
};


#endif // BRIGHTNESSWIDGET_H
