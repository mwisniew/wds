
#ifndef DISPLAYWIDGET_HH
#define DISPLAYWIDGET_HH

#include <QWidget>
#include "ui_displaywidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class DisplayWidget; }
QT_END_NAMESPACE

class DisplayWidget : public QWidget, private Ui::DisplayWidget
{
Q_OBJECT
public:
    /** \brief Display widget constructor
     *
     * \param parent - pointer to widget's parent
     *
     */
    DisplayWidget(QWidget *parent = nullptr);
    /** \brief Destructor
     *
     */
    ~DisplayWidget() {}

    /** \brief Value change step setter
     *
     * \param step - new step value
     *
     */
    void setStep(const double & step) { m_step = step; }

public slots:
    /** \brief Display new value
     *
     * \param val - new value to be displayed
     *
     */
    void display(double val);
    /** \brief Display new state
     *
     * \param val - values greater than 0 display "ON", other display "OFF"
     *
     */
    void displayState(int val);

protected:
    /** \brief Step size to change display colour
     *
     */
    double m_step;

};

#endif // DISPLAYWIDGET_HH
