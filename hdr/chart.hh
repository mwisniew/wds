
#ifndef CHART_HH
#define CHART_HH
#include <QChart>
#include <QValueAxis>
#include <QSplineSeries>
#include <chrono>

QT_BEGIN_NAMESPACE
class QSplineSeries;
class QValueAxis;
QT_END_NAMESPACE

QT_USE_NAMESPACE

class Chart: public QChart
{
    Q_OBJECT
public:
    /** \brief Chart constructor
     *
     * \param name - series name for legend
     * \param parent - pointer to widget's parent
     * \param wFlags - flags required by QChart class
     *
     */
    Chart(const QString & name, QGraphicsItem *parent = nullptr, Qt::WindowFlags wFlags = {});

    /** \brief Destructor
     *
     */
    ~Chart();

    /** \brief Change displayed chart axis ranges
     *
     * \param min_x - minimal value on x axis
     * \param max_x - maximal value on x axis
     * \param min_y - minimal value on y axis
     * \param max_y - maximal value on y axis
     *
     */
    void changeRanges(const double & min_x, const double & max_x, const double & min_y, const double & max_y);

    /** \brief Change series name
     *
     * \param name - series name for legend
     *
     */
    void setName(const QString & name);

public slots:
    /** \brief Add new point to series
     *
     * \param data - new value to be added
     *
     */
    void newData(double data);
    /** \brief Add new point to series
     *
     * \param data - new value to be added
     *
     */
    void newData(int data);
private:
    QSplineSeries *m_series;
    QValueAxis *m_axisX;
    QValueAxis *m_axisY;
    double m_x;
    double m_y;
    std::chrono::time_point<std::chrono::steady_clock> m_prevDataTime;
    std::chrono::time_point<std::chrono::steady_clock> m_start;
};
#endif // CHART_HH
