#ifndef TRANSLATIONSMACROS_HH
#define TRANSLATIONSMACROS_HH

#define MAKE_TRANSLATABLE(BaseClass) \
protected: \
    void changeEvent(QEvent * event) override{ \
        if (event->type() == QEvent::LanguageChange) \
        retranslateUi(this); \
        BaseClass::changeEvent(event); \
}
#define MAKE_TRANSLATABLE_F(BaseClass, TranslateMethod) \
protected: \
    void changeEvent(QEvent * event) override{ \
        if (event->type() == QEvent::LanguageChange) \
        TranslateMethod(); \
        BaseClass::changeEvent(event); \
}

#endif // TRANSLATIONSMACROS_HH
