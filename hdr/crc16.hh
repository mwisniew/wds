
#ifndef CRC16_HH
#define CRC16_HH

#include <cstdint>
#define PN 0x1021

class CRC{
private:
    uint16_t crc;
public:
    /** \brief CRC class constructor
     *
     */
    CRC() : crc(0) {}
    /** \brief CRC value getter
     *
     * \returns Calculated CRC value
     *
     */
    const uint16_t & getCRC() const { return crc; }
    /** \brief calculate CRC from one byte
     *
     * \param byte - byte of data to be calculated
     *
     */
    void calcByte(const uint8_t & byte);
    /** \brief calculate CRC from data
     *
     * \param array - bytes of data to be calculated
     * \param length - length of given array
     *
     */
    void calcByteArray(const char* array, uint16_t length);
};


#endif // CRC16_HH
