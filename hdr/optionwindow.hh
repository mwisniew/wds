
#ifndef OPTIONWINDOW_HH
#define OPTIONWINDOW_HH

#include <QDialog>
#include <QSerialPort>
#include <QIntValidator>
#include <QEvent>
#include <QLineEdit>
#include <QSerialPortInfo>
#include "ui_optionwindow.h"
#include "translationsmacros.hh"

QT_BEGIN_NAMESPACE
namespace Ui { class OptionWindow; }
class QIntValidator;
QT_END_NAMESPACE

class OptionWindow : public QDialog, private Ui::OptionWindow
{
    Q_OBJECT
    MAKE_TRANSLATABLE_F(QDialog, translateUI)
public:
    /** \brief Struct for holding settings
     *
     */
    struct Settings {
        QString name;
        qint32 baudRate;
        QString stringBaudRate;
        QSerialPort::DataBits dataBits;
        QString stringDataBits;
        QSerialPort::Parity parity;
        QString stringParity;
        QSerialPort::StopBits stopBits;
        QString stringStopBits;
        QSerialPort::FlowControl flowControl;
        QString stringFlowControl;
    };
    /** \brief Options window constructor
     *
     * \param parent - pointer to window's parent
     *
     */
    explicit OptionWindow (QWidget *parent = nullptr);
    /** \brief Destructor
     *
     */
    ~OptionWindow();
    /** \brief Get current settings
     *
     * \param newValue - new temperature
     * \returns struct of currently chosen options
     */
    Settings settings() const;

signals:
    /** \brief Signal language change from combo box
     *
     * \param ind - index of selected language
     *
     */
    void languageChange(int ind);

private slots:
    void showPortInfo(int idx);
    void apply();
    void checkCustomBaudRatePolicy(int idx);
    void checkCustomDevicePathPolicy(int idx);
    void langChangeSlot(int idx);

private:
    void fillPortsParameters();
    void fillPortsInfo();
    void updateSettings();
    void translateUI();

    Settings m_currentSettings;
    QIntValidator *m_intValidator = nullptr;
};

#endif // OPTIONWINDOW_HH
