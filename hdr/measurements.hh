
#ifndef MEASUREMENTS_HH
#define MEASUREMENTS_HH

#include <cstdint>

struct Measurements{
public:
    /** \brief Measured temperature
     *
     */
    float m_temperature;

    /** \brief Measured humidity
     *
     */
    int16_t m_humidity;

    /** \brief Measured brightness
     *
     */
    int16_t m_brightness;

    /** \brief Calculated heater power
     *
     */
    float m_heater_control;

    /** \brief Humidifier state
     *
     */
    int m_humi_state;

    /** \brief Calculated lamp power
     *
     */
    int16_t m_lamp_control;

    /** \brief Received crc16 checksum
     *
     */
    uint16_t m_crc_received;

    /** \brief Flag to signal if received data is correct
     *
     */
    bool m_correct;
};

#endif // MEASUREMENTS_HH
