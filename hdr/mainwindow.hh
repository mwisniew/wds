
#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <iostream>
#include <QMainWindow>
#include <QSerialPort>
#include <QDialog>
#include <QTimer>
#include <QTranslator>
#include <QEvent>
#include <Thread>
#include <future>
#include <QChartView>
#include <QDir>
#include <vector>
#include <QMessageBox>
#include "ui_mainwindow.h"
#include "crc16.hh"
#include "chart.hh"
#include "optionwindow.hh"
#include "measurements.hh"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
class QTimer;
QT_END_NAMESPACE

class MainWindow : public QMainWindow, private Ui::MainWindow

{
    Q_OBJECT

public:
    /** \brief Main window constructor
     *
     * \param parent - pointer to window's parent
     *
     */
    MainWindow(QWidget *parent = nullptr);

    /** \brief Destructor
     *
     */
    ~MainWindow();

    virtual void changeEvent(QEvent* event) override;
    virtual void paintEvent(QPaintEvent *pEvent) override;

private slots:
    void openSerialPort();
    void closeSerialPort();
    void writeData(const QByteArray &data);
    void readData();
    void handleError(QSerialPort::SerialPortError error);
    void handleBytesWritten(qint64 bytes);
    void handleWriteTimeout();
    void handleLanguageChange(int ind);


signals:
    /** \brief Signal received temperature change
     *
     * \param newValue - new temperature
     *
     */
    void temperatureValueChanged(double newValue);

    /** \brief Signal received humidity change
     *
     * \param newValue - new humidity
     *
     */
    void humidityValueChanged(int newValue);

    /** \brief Signal received brightness change
     *
     * \param newValue - new brightness
     *
     */
    void lightValueChanged(int newValue);

    /** \brief Signal received heater control change
     *
     * \param newValue - new heater power
     *
     */
    void heaterValueChanged(double newValue);

    /** \brief Signal received humidifier state change
     *
     * \param newValue - new humidifier state
     *
     */
    void humidifierStateChanged(int newValue);

    /** \brief Signal received light control change
     *
     * \param newValue - new lamp power
     *
     */
    void lampValueChanged(double newValue);

   // void

private:
    Measurements readFrame();

    std::mutex m_busyPort;
    qint64 m_bytesToWrite = 0;
    QTimer *m_timer = nullptr;
    QSerialPort *m_serial = nullptr;
    OptionWindow *m_settings = nullptr;
    std::vector<Chart*> m_charts;
    std::vector<QDialog*> m_chartWindows;


};

#endif // MAINWINDOW_HH
