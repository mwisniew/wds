
#ifndef TEMPERATUREWIDGET_H
#define TEMPERATUREWIDGET_H

#include <QWidget>
#include <QPainter>
#include "ui_thermometer.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Thermometer; }
QT_END_NAMESPACE

class TemperatureWidget : public QWidget, private Ui::Thermometer
{
    Q_OBJECT
public:
    /** \brief Thermometer widget constructor
     *
     * \param parent - pointer to widget's parent
     *
     */
    TemperatureWidget(QWidget *parent = nullptr);

    /** \brief Destructor
     *
     */
    ~TemperatureWidget() {}

public slots:
    /** \brief Display new value and change thermometer bar level
     *
     * \param val - new value to be displayed
     *
     */
    void display(double val);

private:
    double m_max;
    double m_min;

    double m_offset;
    double m_mult;

};


#endif // TEMPERATUREWIDGET_H
