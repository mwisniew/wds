var hierarchy =
[
    [ "Ui::BrightnessWidget", null, [
      [ "BrightnessWidget", "class_brightness_widget.html", null ]
    ] ],
    [ "CRC", "class_c_r_c.html", null ],
    [ "Ui::DisplayWidget", null, [
      [ "DisplayWidget", "class_display_widget.html", null ]
    ] ],
    [ "Ui::FogWidget", null, [
      [ "HumidityWidget", "class_humidity_widget.html", null ]
    ] ],
    [ "Ui::MainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "Measurements", "struct_measurements.html", null ],
    [ "Ui::OptionWindow", null, [
      [ "OptionWindow", "class_option_window.html", null ]
    ] ],
    [ "QChart", null, [
      [ "Chart", "class_chart.html", null ]
    ] ],
    [ "QDialog", null, [
      [ "OptionWindow", "class_option_window.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "MainWindow", "class_main_window.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "BrightnessWidget", "class_brightness_widget.html", null ],
      [ "DisplayWidget", "class_display_widget.html", null ],
      [ "HumidityWidget", "class_humidity_widget.html", null ],
      [ "TemperatureWidget", "class_temperature_widget.html", null ]
    ] ],
    [ "OptionWindow::Settings", "struct_option_window_1_1_settings.html", null ],
    [ "Ui::Thermometer", null, [
      [ "TemperatureWidget", "class_temperature_widget.html", null ]
    ] ]
];