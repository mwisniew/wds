var annotated_dup =
[
    [ "BrightnessWidget", "class_brightness_widget.html", "class_brightness_widget" ],
    [ "Chart", "class_chart.html", "class_chart" ],
    [ "CRC", "class_c_r_c.html", "class_c_r_c" ],
    [ "DisplayWidget", "class_display_widget.html", "class_display_widget" ],
    [ "HumidityWidget", "class_humidity_widget.html", "class_humidity_widget" ],
    [ "MainWindow", "class_main_window.html", "class_main_window" ],
    [ "Measurements", "struct_measurements.html", "struct_measurements" ],
    [ "OptionWindow", "class_option_window.html", "class_option_window" ],
    [ "TemperatureWidget", "class_temperature_widget.html", "class_temperature_widget" ]
];