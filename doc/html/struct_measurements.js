var struct_measurements =
[
    [ "m_brightness", "struct_measurements.html#a6dd816839d18641704a1cde3867b8326", null ],
    [ "m_correct", "struct_measurements.html#aedf1da11d190f39d267134e1822592b5", null ],
    [ "m_crc_received", "struct_measurements.html#a817f9d533021dac0c6e9dc8a54381504", null ],
    [ "m_heater_control", "struct_measurements.html#a825dd199746f359564f1ea7e5352d4ae", null ],
    [ "m_humi_state", "struct_measurements.html#a6538ae8b9a73a8574fa1fae125e815f9", null ],
    [ "m_humidity", "struct_measurements.html#a65c9a3839fdec215604da4892e599cae", null ],
    [ "m_lamp_control", "struct_measurements.html#a34c94d3f13c5888759603fdddfd3e55e", null ],
    [ "m_temperature", "struct_measurements.html#accc3d5cbbbe297d1f481f9fa8041dd16", null ]
];