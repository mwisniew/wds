var class_main_window =
[
    [ "MainWindow", "class_main_window.html#a996c5a2b6f77944776856f08ec30858d", null ],
    [ "~MainWindow", "class_main_window.html#ae98d00a93bc118200eeef9f9bba1dba7", null ],
    [ "heaterValueChanged", "class_main_window.html#a6699bc063f447b97d4c67f49c9a6ab80", null ],
    [ "humidifierStateChanged", "class_main_window.html#af67cefbd1e2f36d6881d765fc3a20a31", null ],
    [ "humidityValueChanged", "class_main_window.html#ac10c14218cf3b42ddfd5dc42fae638ec", null ],
    [ "lampValueChanged", "class_main_window.html#ad993523216633e0537f02636863cc514", null ],
    [ "lightValueChanged", "class_main_window.html#a88be0f91f8f79464eb46a5bdad836077", null ],
    [ "temperatureValueChanged", "class_main_window.html#ab73fd84087f9a0e831907ef2743079a5", null ]
];